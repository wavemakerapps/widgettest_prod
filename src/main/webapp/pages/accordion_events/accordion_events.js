Application.$controller("accordion_eventsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.accordionpane2Load = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.title = $scope.Widgets.text1.datavalue;
        $isolateScope.subheading = $scope.Widgets.text2.datavalue;
        $isolateScope.badgevalue = $scope.Widgets.text3.datavalue;
        $isolateScope.badgetype = $scope.Widgets.select1.datavalue;
        $isolateScope.iconclass = $scope.Widgets.text5.datavalue;
        $isolateScope.content = $scope.Widgets.text6.datavalue;
        $isolateScope.height = $scope.Widgets.text11.datavalue;
    };

    $scope.accordionpane2Expand = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.subheading = $event.type + " triggered";
    };

    $scope.accordionpane2Collapse = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.subheading = $event.type + " triggered";
    };

    $scope.accordion1Change = function($event, $isolateScope, newPaneIndex, oldPaneIndex) {
        debugger;
        console.log($event.type);
        if (newPaneIndex === 2)
            $isolateScope.closeothers = true;
        else if (newPaneIndex === 3)
            $isolateScope.closeothers = false;
        $scope.Widgets.accordionpane5.title = "Accordion changed to " + newPaneIndex + " from " +
            oldPaneIndex;
    };

}]);