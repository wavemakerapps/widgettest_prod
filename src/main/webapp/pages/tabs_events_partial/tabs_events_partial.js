Application.$controller("tabs_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.tabs1Change = function($event, $isolateScope, newPaneIndex, oldPaneIndex) {
        debugger;
        console.log($event.type);
        if (newPaneIndex === 2)
            $isolateScope.tabsposition = "top";
        else if (newPaneIndex === 3)
            $isolateScope.tabsposition = "bottom";
        $scope.Widgets.tabpane5.title = "Tab changed to " + newPaneIndex + " from " +
            oldPaneIndex;
    };

    $scope.tabpane3Load = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.title = $scope.Widgets.text1.datavalue;
        $isolateScope.badgevalue = $scope.Widgets.text3.datavalue;
        $isolateScope.badgetype = $scope.Widgets.select1.datavalue;
        $isolateScope.iconclass = $scope.Widgets.text5.datavalue;
        $isolateScope.content = $scope.Widgets.text6.datavalue;
    };

    $scope.tabpane4Select = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.title = $event.type + " triggered";
    };

    $scope.tabpane5Deselect = function($event, $isolateScope) {
        debugger;
        console.log($event.type);
        $isolateScope.title = $event.type + " triggered";
    };

}]);