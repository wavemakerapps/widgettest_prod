Application.$controller("radioset_scriptPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };




    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.selectedvalue = $isolateScope.datavalue;
    };


    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.dataset = $isolateScope.datavalue;
    };


    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.readonly = $isolateScope.datavalue;
    };


    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.disabled = $isolateScope.datavalue;
    };


    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.show = $isolateScope.datavalue;
    };


    $scope.text7_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset7.datavalue = $isolateScope.datavalue;
    };


    $scope.select2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.layout = $isolateScope.datavalue;
    };


    $scope.text8_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset1.usekeys = $isolateScope.datavalue;
    };


    $scope.text9Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.datafield = $isolateScope.datavalue;
    };


    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset1.displayfield = $isolateScope.datavalue;
    };


    $scope.text11Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset1.displayexpression = $isolateScope.datavalue;
    };


    $scope.text12_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset1.orderby = $isolateScope.datavalue;
    };


    $scope.text7Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.width = $isolateScope.datavalue;
    };


    $scope.text13Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.height = $isolateScope.datavalue;
    };


    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.radioset2.required = $isolateScope.datavalue;
    };

}]);