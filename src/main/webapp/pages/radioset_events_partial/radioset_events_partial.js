Application.$controller("radioset_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.radioset1Change = function($event, $isolateScope, newVal, oldVal) {
        console.log($event.type);
        $isolateScope.layout = "stacked";
    };

    $scope.radioset1Click = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.dataset = $scope.Widgets.text12.datavalue;
        $isolateScope.displayexpression = $scope.Widgets.text11.datavalue;
        $isolateScope.height = $scope.Widgets.text13.datavalue;
        $isolateScope.orderby = $scope.Widgets.text12_1.datavalue;
        $isolateScope.datavalue = $scope.Widgets.select1.datavalue;
        $isolateScope.datafield = $scope.Widgets.text9.datavalue;
    };


    $scope.radioset1Mouseenter = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.layout = "stacked";
        $isolateScope.width = "100px";
        $isolateScope.height = "100px";
        $isolateScope.orderby = "Zip:asc";
        $isolateScope.displayexpression = "$[dataValue]";
        $isolateScope.selectedvalue = "Koc";
    };


    $scope.radioset1Mouseleave = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.disabled = $scope.Widgets.toggle3.datavalue;
    };


    $scope.radioset1Tap = function($event, $isolateScope) {
        console.log($event.type);
        $isolateScope.orderby = $scope.Widgets.text12_1.datavalue;
        $isolateScope.datavalue = $scope.Widgets.select1.datavalue;
        $isolateScope.width = $scope.Widgets.text7.datavalue;
    };

}]);