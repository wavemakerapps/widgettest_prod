Application.$controller("accordion_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane1.title = $isolateScope.datavalue;
    };

    $scope.text2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.subheading = $isolateScope.datavalue;
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.badgevalue = $isolateScope.datavalue;
    };

    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.badgetype = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.show = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.iconclass = $isolateScope.datavalue;
    };

    $scope.text6Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.content = $isolateScope.datavalue;
    };

    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordion1.show = $isolateScope.datavalue;
    };

    $scope.text18Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordion1.defaultpaneindex = $isolateScope.datavalue;
    };

    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.deferload = $isolateScope.datavalue;
    };

    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordion1.deferload = $isolateScope.datavalue;
    };

    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordion1.closeothers = $isolateScope.datavalue;
    };

    $scope.text11Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordionpane2.height = $isolateScope.datavalue;
    };

    $scope.text12Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.accordion1.height = $isolateScope.datavalue;
    };

}]);