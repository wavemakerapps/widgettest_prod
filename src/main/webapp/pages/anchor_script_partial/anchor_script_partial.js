Application.$controller("anchor_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.caption = $isolateScope.datavalue;
    };

    $scope.text2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.badgevalue = $isolateScope.datavalue;
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.hint = $isolateScope.datavalue;
    };

    $scope.select1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.hyperlink = $isolateScope.datavalue;
    };

    $scope.select2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.target = $isolateScope.datavalue;
    };

    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.show = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconclass = $isolateScope.datavalue;
    };

    $scope.text6Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconurl = $isolateScope.datavalue;
    };

    $scope.text13Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.encodeurl = $isolateScope.datavalue;
    };

    $scope.text14Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconwidth = $isolateScope.datavalue;
    };

    $scope.text14_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconheight = $isolateScope.datavalue;
    };

    $scope.text15Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconmargin = $isolateScope.datavalue;
    };

    $scope.select3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.iconposition = $isolateScope.datavalue;
    };
    $scope.text15_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.height = $isolateScope.datavalue;
    };
    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.anchor1.width = $isolateScope.datavalue;
    };


}]);