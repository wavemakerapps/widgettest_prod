Application.$controller("iframedialog_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);


Application.$controller("iframedialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.iframedialog1Ok = function($event, $isolateScope) {
            alert("Iframe Button Action Performed")
        };

        $scope.iframedialog1Close = function($event, $isolateScope) {
            alert("Iframe Closed X");
        };

        $scope.iframedialog1Opened = function($event, $isolateScope) {
            console.log("Open");
            $isolateScope.title = $scope.Widgets.text3.datavalue;
            $isolateScope.oktext = $scope.Widgets.text9.datavalue;
            $isolateScope.width = $scope.Widgets.text12_1.datavalue;
            $isolateScope.height = $scope.Widgets.text9_1.datavalue;
            $isolateScope.showheader = $scope.Widgets.toggle3.datavalue;
            $isolateScope.url = $scope.Widgets.select1.datavalue;
            $isolateScope.modal = $scope.Widgets.toggle1.datavalue;
            $isolateScope.closable = $scope.Widgets.toggle2.datavalue;
            $isolateScope.showactions = $scope.Widgets.toggle4.datavalue;
            $isolateScope.encodeurl = $scope.Widgets.toggle5.datavalue;
            $isolateScope.iconclass = $scope.Widgets.text12.datavalue;
            $isolateScope.iconwidth = $scope.Widgets.text10.datavalue;
            $isolateScope.iconheight = $scope.Widgets.text11.datavalue;
            $isolateScope.iconmargin = $scope.Widgets.text12_2.datavalue;
        };

    }
]);