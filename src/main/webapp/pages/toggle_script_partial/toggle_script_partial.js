Application.$controller("toggle_script_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.text3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.caption = $isolateScope.datavalue;
        $scope.Widgets.toggle1.hint = $isolateScope.datavalue;
    };

    $scope.toggle2Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.show = $isolateScope.datavalue;
    };

    $scope.text5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.datavalue = $isolateScope.datavalue;
    };

    $scope.toggle3Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.readonly = $isolateScope.datavalue;
    };

    $scope.toggle4Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.disabled = $isolateScope.datavalue;
    };

    $scope.toggle5Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.required = $isolateScope.datavalue;
    };

    $scope.text13Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.checkedvalue = $isolateScope.datavalue;
    };

    $scope.text14Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.uncheckedvalue = $isolateScope.datavalue;
    };

    $scope.text10Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.width = $isolateScope.datavalue;
    };

    $scope.text12_1Change = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.toggle1.height = $isolateScope.datavalue;
    };

}]);