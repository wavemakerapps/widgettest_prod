Application.$controller("html_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.html1Doubletap = function($event, $isolateScope) {
        $isolateScope.hint = "Double Tap";
        console.log("Double Tap");
    };
    $scope.html1Tap = function($event, $isolateScope) {
        $isolateScope.hint = "Tap";
        console.log("Tap");
    };
    $scope.html1Mouseleave = function($event, $isolateScope) {
        $isolateScope.content = "Mouse Leave";
        console.log("Mouse Leave");
    };
    $scope.html1Mouseenter = function($event, $isolateScope) {
        $isolateScope.content = "Mouse Enter";
        console.log("Mouse Enter");
    };
    $scope.html1Dblclick = function($event, $isolateScope) {
        $isolateScope.content = "Double Click";
        console.log("Double Click");
    };
    $scope.html1Click = function($event, $isolateScope) {
        //debugger;
        //call variables and alerts
        console.log(Partial);
        $isolateScope.hint = $scope.Widgets.text3.datavalue;
        $isolateScope.show = $scope.Widgets.toggle1.datavalue;
        $isolateScope.content = $scope.Widgets.textarea1.datavalue;
        console.log("Click");
    };

}]);