Application.$controller("toggle_events_partialPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

    $scope.toggle1Tap = function($event, $isolateScope) {
        $isolateScope.caption = "Tap";
        $isolateScope.datavalue = $scope.Widgets.text5.datavalue;
        $isolateScope.checkedvalue = $scope.Widgets.text3.datavalue;
        $isolateScope.disabled = $scope.Widgets.toggle4.datavalue;
        $isolateScope.required = $scope.Widgets.toggle5.datavalue;
        console.log("Tap");
    };
    $scope.toggle1Mouseleave = function($event, $isolateScope) {
        $isolateScope.caption = "Leave";
        $isolateScope.disabled = true;
        console.log("Mouse Leave");
    };
    $scope.toggle1Mouseenter = function($event, $isolateScope) {
        $isolateScope.caption = "Enter";
        $isolateScope.disabled = false;
        console.log("Mouse enter");
    };
    $scope.toggle1Click = function($event, $isolateScope) {
        $isolateScope.caption = "Click";
        $isolateScope.hint = $scope.Widgets.text3.datavalue;
        $isolateScope.uncheckedvalue = $scope.Widgets.text3.datavalue;
        $isolateScope.show = $scope.Widgets.toggle2.datavalue;
        $isolateScope.readonly = $scope.Widgets.toggle3.datavalue;
        console.log("Click");
    };
    $scope.toggle1Blur = function($event, $isolateScope) {
        $isolateScope.caption = "Blur";
        $isolateScope.required = false;
        console.log("Blur");
    };
    $scope.toggle1Focus = function($event, $isolateScope) {
        $isolateScope.caption = "Focus";
        $isolateScope.required = true;
        console.log("Focus");
    };
    $scope.toggle1Change = function($event, $isolateScope, newVal, oldVal) {
        console.log("Change");
        console.log(newVal + " / " + oldVal);
        alert(oldVal + " changed to " + newVal);
        //call action variables 
    };
}]);